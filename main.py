from modules.scope import Scope
from source.appdata import AppData

if Scope(__name__).equals('main'):

    def setup():
        appData = AppData(__file__)
        return appData

    def runMain():
        from source.app import App
        app = App()
        appData = setup()
        app.execute(appData)

    runMain()