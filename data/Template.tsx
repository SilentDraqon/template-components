import React from "react";
import { setClass } from "../../modules/setClass";
import { TemplateProps } from "./Template.types";
import texts from "./Template.json"
import "./Template.scss";

const Template = ({language, theme, className}: TemplateProps) => {

    return (
        <div className={setClass("hw_template", [theme], className)}>
            template
        </div>
    )
}

export default Template
