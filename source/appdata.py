from modules.argparse import getConsoleArgs
from modules.dir import PointerDir, ProjectDir

class AppData:

    def __init__(self, mainfile):
        self.consoleArgs = getConsoleArgs()
        self.projectDir = ProjectDir(mainfile)
        self.pointerDir = PointerDir()