from modules.file import File

inName = "Template"
inDir = "data"
filetypes = ["scss", "tsx", "types.ts", "json"]

class App:

    def initiliaze(self, data):
        self.data = data
        
    def execute(self, data):
        self.initiliaze(data)
        m_files = self.getFiles()
        self.replaceFilesContents(m_files)

    def getFiles(self, results=[]):
        for f_end in filetypes:
            inputpath = "\\"+inDir+"\\"
            outdir = self.data.consoleArgs.outdir
            outputpath = "\\"+outdir+"\\"+''.join(self.data.consoleArgs.component.split())+"\\"
            results.append({
                "input": File(self.data.projectDir.seek(inputpath, inName+"."+f_end)),
                "output": File(self.data.projectDir.seek(outputpath, ''.join((self.data.consoleArgs.component+"."+f_end).split())))})
        return results

    def replaceFilesContents(self, m_files):
        newKeyword = ''.join(self.data.consoleArgs.component.split())

        self.data.consoleArgs.whitespace

        for m_file in m_files:
            m_data= m_file["input"].read()
            m_data = m_data.replace(inName, newKeyword)
            m_data = m_data.replace(inName.lower(), newKeyword.lower())
            m_file["output"].write(m_data) 