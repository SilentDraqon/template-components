import argparse

def getConsoleArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument("--component", default="NewComponent", help="name of output component")
    parser.add_argument("--outdir", default="out", help="name of output component")
    parser.add_argument("--whitespace", default="", help="following string will replace whitespaces when renaming (usually empty, dash or underscore)")
    return parser.parse_args()