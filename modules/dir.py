import os
import pathlib
from .userinput import userProceeds

class Dir():

    def locationExists(self, additionalPath):
        exists = os.path.isdir(self.path + additionalPath)    
        return exists 

    def createSubDirs(self, path):
        os.makedirs(path)

    def seek(self, additionalPath, file=""):
        if not self.locationExists(additionalPath):
            if userProceeds("Dir doesnt exist, do you want to create it?"):
                self.createSubDirs(self.path + additionalPath)
        return self.path + additionalPath + file


class ProjectDir(Dir):
    
    def __init__(self, main_file_path):
        self.path = main_file_path.removesuffix('\\main.py')
        
class PointerDir(Dir):

    def __init__(self):
        self.path = (str(pathlib.Path().resolve()))