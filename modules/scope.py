class Scope:

    def __init__(self, namespace):
        self.namespace = namespace

    def equals(self, name):
        return self.namespace == '__'+name+'__'