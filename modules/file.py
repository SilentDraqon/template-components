import json

class File():

    def __init__(self, path):
        self.filePath = path

    def write(self, data):
        f = open(self.filePath, "w")
        f.write(data) 

    def read(self):
        f = open(self.filePath, "r")
        return f.read()

class JsonFile(File):

    def write(self, data):
        open(self.filePath, "w").write(
            str(json.dumps(
                data, 
                indent=4, 
                sort_keys=True)))

    def read(self):
        return json.loads(open(self.filePath, "r").read())