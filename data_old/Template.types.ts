export interface TemplateProps  {
    language?: "english" | "german"
    theme?: "light" | "dark"
    className?: string
}